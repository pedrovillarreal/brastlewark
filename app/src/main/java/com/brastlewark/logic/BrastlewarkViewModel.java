package com.brastlewark.logic;

import android.view.View;

import com.brastlewark.activity.BrastlewarkActivity;
import com.brastlewark.domain.Brastlewark;
import com.brastlewark.domain.Census;
import com.brastlewark.service.interfaces.ServiceCallback;
import com.brastlewark.service.services.Services;

import java.util.ArrayList;

public class BrastlewarkViewModel {
    private BrastlewarkActivity brastlewarkActivity;
    private ArrayList<Brastlewark> visibleBrastlewarks;
    private ArrayList<Brastlewark> brastlewarks;

    public BrastlewarkViewModel(BrastlewarkActivity brastlewarkActivity) {
        this.brastlewarkActivity = brastlewarkActivity;
        visibleBrastlewarks = new ArrayList<>();
        brastlewarks = new ArrayList<>();
        loadCensusBrastlewark();
    }

    private void loadCensusBrastlewark() {
        brastlewarkActivity.getProgressBar().setVisibility(View.VISIBLE);
        try {
            Services.getCensusBrastlewark(new ServiceCallback<Census>() {
                @Override
                public void response(Census response, Exception e) {
                brastlewarkActivity.getProgressBar().setVisibility(View.INVISIBLE);
                    if (e != null) {
                        e.printStackTrace();
                        return;
                    }
                    brastlewarks.addAll(response.getBrastlewark());
                    visibleBrastlewarks.addAll(response.getBrastlewark());
                    brastlewarkActivity.updateAdapter();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Brastlewark> getVisibleBrastlewarks() {
        return visibleBrastlewarks;
    }

    public void setVisibleBrastlewarks(ArrayList<Brastlewark> visibleBrastlewarks) {
        this.visibleBrastlewarks = visibleBrastlewarks;
    }

    public ArrayList<Brastlewark> getBrastlewarks() {
        return brastlewarks;
    }

    public void setBrastlewarks(ArrayList<Brastlewark> brastlewarks) {
        this.brastlewarks = brastlewarks;
    }
}
