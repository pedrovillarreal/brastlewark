package com.brastlewark.service.services;

import com.brastlewark.domain.Census;
import com.brastlewark.service.ApiClient;
import com.brastlewark.service.interfaces.BrastlewarkServicesInterfaces;
import com.brastlewark.service.interfaces.ServiceCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Services {
    public static void getCensusBrastlewark(final ServiceCallback<Census> callback) {
        BrastlewarkServicesInterfaces apiService= ApiClient.getClient().create(BrastlewarkServicesInterfaces.class);
        Call<Census> call= apiService.getBrastlewarks();
        call.enqueue(new Callback<Census>() {

            @Override
            public void onResponse(Call<Census> call, Response<Census> response) {

                try{
                    if (response.errorBody() == null ) {
                        Census brastlewarks = response.body();
                        callback.response(brastlewarks, null);
                        return;
                    }
                    else
                        callback.response(null, new Exception());

                }
                catch (Exception e) {
                    callback.response(null,e);
                }

            }
            @Override
            public void onFailure(Call<Census> call, Throwable t) {
                Exception e = (t instanceof Exception) ? (Exception) t : new Exception(t);
                callback.response(null,  e);
            }
        });
    }
}
