package com.brastlewark.service.interfaces;


public interface ServiceCallback<T> {
    void response(T response, Exception e);
}
