package com.brastlewark.service.interfaces;

import com.brastlewark.domain.Census;
import retrofit2.Call;
import retrofit2.http.GET;

public interface BrastlewarkServicesInterfaces {
    @GET("/rrafols/mobile_test/master/data.json")
    Call<Census> getBrastlewarks();
}
