package com.brastlewark.activity.adapters;

import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TableRow;
import android.widget.TextView;

import com.brastlewark.R;
import com.brastlewark.activity.component.AlertDialogBrastlewarkFragment;
import com.brastlewark.domain.Brastlewark;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;

public class BrastlewarkAdapter extends RecyclerView.Adapter<BrastlewarkAdapter.ViewHolder> {

    private ArrayList<Brastlewark> brastlewarkArrayList;
    private static Context context;

    public BrastlewarkAdapter(Context context, ArrayList<Brastlewark> brastlewarks) {
        super();
        this.context = context;
        this.brastlewarkArrayList = brastlewarks;
    }

    @Override
    public BrastlewarkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return make(this, parent, viewType);
    }


    private BrastlewarkAdapter.ViewHolder make(BrastlewarkAdapter adapter, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        BrastlewarkAdapter.ViewHolder viewHolder = null;
        try {
            View card = inflater.inflate(R.layout.cardview_brastlewark, viewGroup, false);
            viewHolder = new BrastlewarkAdapter.CardViewBrastlewarkHolderHolder(card, adapter);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BrastlewarkAdapter.ViewHolder viewHolder, int position) {
         try {
             BrastlewarkAdapter.CardViewBrastlewarkHolderHolder brastlewarkHolder =
                     (BrastlewarkAdapter.CardViewBrastlewarkHolderHolder) viewHolder;

             Brastlewark brastlewark = brastlewarkArrayList.get(position);
             brastlewarkHolder.setBrastlewark(brastlewark);

             if (brastlewarkArrayList.get(position) == null)
                 return;

             setSizeCardView(brastlewarkHolder.container);
             brastlewarkHolder.name.setText(brastlewark.getName());
             brastlewarkHolder.age.setText(String.valueOf(brastlewark.getAge()));
             brastlewarkHolder.hairColor.setText(brastlewark.getHairColor());
             if (brastlewark.getThumbnail() != null && !brastlewark.getThumbnail().isEmpty())
                 setAssetImage(brastlewarkHolder.thumnail, brastlewark.getThumbnail());
         } catch (Exception e) {
             e.printStackTrace();
         }
    }

    @Override
    public int getItemCount() {
        return brastlewarkArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView container;
        protected final BrastlewarkAdapter adapter;
        private Brastlewark brastlewark;

        public ViewHolder(View itemView, BrastlewarkAdapter adapter) {
            super(itemView);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
        }

        public Brastlewark getBrastlewark() {
            return brastlewark;
        }

        public void setBrastlewark(Brastlewark brastlewark) {
            this.brastlewark = brastlewark;
        }
    }

    private class CardViewBrastlewarkHolderHolder extends BrastlewarkAdapter.ViewHolder {
        public SimpleDraweeView thumnail;
        public TextView name;
        public TextView age;
        public TextView hairColor;

        CardViewBrastlewarkHolderHolder(View itemView, BrastlewarkAdapter adapter) {
            super(itemView, adapter);
            container = itemView.findViewById(R.id.cardview);
            thumnail = itemView.findViewById(R.id.thumbnail);
            name = itemView.findViewById(R.id.name);
            age = itemView.findViewById(R.id.age);
            hairColor = itemView.findViewById(R.id.hair_color);
        }

        @Override
        public void onClick(View view) {
            initAlertDialog(getBrastlewark());
        }
    }

    private static void setAssetImage(SimpleDraweeView imageview, String url){
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(url))
                .setResizeOptions(new ResizeOptions(120, 120))
                .build();
        imageview.setController(Fresco.newDraweeControllerBuilder()
                .setOldController(imageview.getController())
                .setImageRequest(request)
                .build());
    }

    private static void setSizeCardView(CardView cardView) {
        // Get screen size
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point windowSize = new Point();
        display.getRealSize(windowSize);
        // calculates width and height of card
        Double width = (double) cardView.getLayoutParams().width;
        Double height = windowSize.x * 0.5 * 1.17;
        // Sets width and height of card
        TableRow.LayoutParams params = new TableRow.LayoutParams(width.intValue(), height.intValue());
        cardView.setLayoutParams(params);

    }

    private void initAlertDialog(Brastlewark brastlewark) {
        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        AlertDialogBrastlewarkFragment dialog = new AlertDialogBrastlewarkFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("brastlewark", brastlewark);

        dialog.setArguments(bundle);
        dialog.show(fragmentManager, "AlertDialogBrastlewarkFragment");
    }
}
