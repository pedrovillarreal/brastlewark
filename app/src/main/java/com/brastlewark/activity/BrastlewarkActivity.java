package com.brastlewark.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.brastlewark.R;
import com.brastlewark.activity.adapters.BrastlewarkAdapter;
import com.brastlewark.activity.component.AlertDialogBrastlewarkFragment;
import com.brastlewark.domain.Brastlewark;
import com.brastlewark.logic.BrastlewarkViewModel;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.ArrayList;

public class BrastlewarkActivity extends AppCompatActivity implements AlertDialogBrastlewarkFragment.AlertDialogBrastlewarkDialogListener {
    private static final int RESULT_CODE_FILTER = 1;
    private static final int FILTER_NAME = 1;
    private static final int FILTER_AGE = 2;
    private static final int FILTER_HAIR_COLOR = 3;

    private BrastlewarkViewModel brastlewarkViewModel;
    private RecyclerView recyclerView;
    private BrastlewarkAdapter adapter;
    private SearchView simpleSearchView;
    private ImageView filter;
    private int filterOption;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Fresco.initialize(this);
            setContentView(R.layout.activity_brastlewark);
            initComponents();
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Brastlewark");
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_brastlewark, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initComponents() {
        try {
            progressBar = findViewById(R.id.progressBar);
            brastlewarkViewModel = new BrastlewarkViewModel(this);
            initRecycler();
            initSearchView();
            initFilterButton();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initFilterOption() {
        filterOption = FILTER_NAME;
    }

    public void initRecycler() {
        recyclerView = findViewById(R.id.recycler);
        adapter = new BrastlewarkAdapter(this, brastlewarkViewModel.getVisibleBrastlewarks());
        LinearLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public void updateAdapter() {
        adapter.notifyDataSetChanged();
    }

    private void initSearchView() {
        simpleSearchView = findViewById(R.id.searchView);
        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchBrastlewekark(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchBrastlewekark(newText);
                return false;
            }
        });
    }

    private void initFilterButton() {
        initFilterOption();
        filter = findViewById(R.id.filter);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                intent.putExtra("filterOption", filterOption);
                startActivityForResult(intent, RESULT_CODE_FILTER);
            }
        });
    }

    private void searchBrastlewekark(String query) {
        ArrayList<Brastlewark> visibleBrastlewark = new ArrayList<>();
        for (Brastlewark brastlewark : brastlewarkViewModel.getBrastlewarks()) {
            switch (filterOption) {
                case FILTER_NAME:
                    if (brastlewark.getName().toLowerCase() != null && brastlewark.getName().toLowerCase().contains(query.toLowerCase().trim()))
                        visibleBrastlewark.add(brastlewark);
                    break;
                case FILTER_AGE:
                    if (String.valueOf(brastlewark.getAge()).toLowerCase() != null && String.valueOf(brastlewark.getAge()).toLowerCase().contains(query.toLowerCase().trim()))
                        visibleBrastlewark.add(brastlewark);
                    break;
                case FILTER_HAIR_COLOR:
                    if (brastlewark.getHairColor().toLowerCase() != null && brastlewark.getHairColor().toLowerCase().contains(query.toLowerCase().trim()))
                        visibleBrastlewark.add(brastlewark);
                    break;
            }
        }

        updateVisibleBrastlewarks(visibleBrastlewark);
    }

    private void updateVisibleBrastlewarks(ArrayList<Brastlewark> visibleBrastlewark) {
        if (visibleBrastlewark.size() > 0) {
            brastlewarkViewModel.getVisibleBrastlewarks().clear();
            brastlewarkViewModel.getVisibleBrastlewarks().addAll(visibleBrastlewark);
            updateAdapter();
        }
    }

    @Override
    public void onDialogCancelClick(DialogFragment dialog) {
        dialog.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RESULT_CODE_FILTER)
            if(resultCode == Activity.RESULT_OK)
                setFilterOption(data.getIntExtra("result", 0));

    }

    private void setFilterOption(int option) {
        filterOption = option;
        if (!simpleSearchView.getQuery().toString().isEmpty())
            searchBrastlewekark(simpleSearchView.getQuery().toString());
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }
}
