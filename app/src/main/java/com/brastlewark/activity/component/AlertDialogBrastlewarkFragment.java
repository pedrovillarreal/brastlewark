package com.brastlewark.activity.component;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.brastlewark.R;
import com.brastlewark.domain.Brastlewark;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.Arrays;

public class AlertDialogBrastlewarkFragment extends DialogFragment {
    private AlertDialogBrastlewarkDialogListener mListener;
    private SimpleDraweeView thumbnail;
    private TextView name;
    private TextView age;
    private TextView hairColor;
    private TextView weight;
    private TextView height;
    private TextView friends;
    private TextView professions;
    private Brastlewark brastlewark;
    private ImageView cancelButton;

    public AlertDialogBrastlewarkFragment(){}

    public AlertDialogBrastlewarkDialogListener getmListener() {
        return mListener;
    }

    public void setmListener(AlertDialogBrastlewarkDialogListener mListener) {
        this.mListener = mListener;
    }

    public TextView getAge() {
        return age;
    }

    public void setAge(TextView age) {
        this.age = age;
    }

    public TextView getHairColor() {
        return hairColor;
    }

    public void setHairColor(TextView hairColor) {
        this.hairColor = hairColor;
    }

    public TextView getWeight() {
        return weight;
    }

    public void setWeight(TextView weight) {
        this.weight = weight;
    }

    public TextView getWidth() {
        return height;
    }

    public void setWidth(TextView width) {
        this.height = width;
    }

    public TextView getFriends() {
        return friends;
    }

    public void setFriends(TextView friends) {
        this.friends = friends;
    }

    public TextView getProfessions() {
        return professions;
    }

    public void setProfessions(TextView professions) {
        this.professions = professions;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }

    public ImageView getCancelButton() {
        return cancelButton;
    }

    public void setCancelButton(ImageView cancelButton) {
        this.cancelButton = cancelButton;
    }

    public interface AlertDialogBrastlewarkDialogListener {
        void onDialogCancelClick(DialogFragment dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.alert_dialog_brastlewark, null);
        thumbnail = view.findViewById(R.id.thumbnail);
        name = view.findViewById(R.id.name);
        age = view.findViewById(R.id.age);
        hairColor = view.findViewById(R.id.hair_color);
        weight = view.findViewById(R.id.weight);
        height = view.findViewById(R.id.height);
        friends = view.findViewById(R.id.friends);
        professions = view.findViewById(R.id.professions);
        cancelButton = view.findViewById(R.id.cancel);
        getParams();

        getCancelButton().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getmListener().onDialogCancelClick(AlertDialogBrastlewarkFragment.this);
            }
        });

        builder.setView(view);
        return builder.create();
    }

    private void getParams(){
        this.brastlewark = (Brastlewark) getArguments().getSerializable("brastlewark");
        setTexts(brastlewark);
    }

    private void setTexts(Brastlewark brastlewark) {
        this.thumbnail.setImageURI(Uri.parse(brastlewark.getThumbnail()));
        this.name.setText(brastlewark.getName());
        this.age.setText(String.valueOf(brastlewark.getAge()));
        this.hairColor.setText(brastlewark.getHairColor());
        this.weight.setText(String.valueOf(brastlewark.getWeight()));
        this.height.setText(String.valueOf(brastlewark.getHeight()));
        this.friends.setText(Arrays.toString(brastlewark.getFriends().toArray()));
        this.professions.setText(Arrays.toString(brastlewark.getProfessions().toArray()));
    }

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            setmListener((AlertDialogBrastlewarkDialogListener) activity);
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AlertDialogBrastlewarkDialogListener");
        }
    }

}
