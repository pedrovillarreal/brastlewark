package com.brastlewark.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.brastlewark.R;

public class FilterActivity extends AppCompatActivity {
    private static final int FILTER_NAME = 1;
    private static final int FILTER_AGE = 2;
    private static final int FILTER_HAIR_COLOR = 3;

    private int selectedOption;
    private RadioGroup radioGroup;
    private Button closeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_filter);
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("Filter");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            initRadioGroup(getIntent().getIntExtra("filterOption", 1));
            initCloseButton();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", selectedOption);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
        return true;
    }

    private void initRadioGroup(int option) {
        radioGroup = findViewById(R.id.radioGroup);
        setCheckedRadioGroup(option);
        selectedOption = option;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.name)
                    selectedOption = FILTER_NAME;
                else if(i == R.id.age)
                    selectedOption = FILTER_AGE;
                else
                    selectedOption = FILTER_HAIR_COLOR;
            }
        });
    }

    private void setCheckedRadioGroup(int option) {
        switch (option) {
            case FILTER_NAME:
                radioGroup.check(R.id.name);
                break;
            case FILTER_AGE:
                radioGroup.check(R.id.age);
                break;
            case FILTER_HAIR_COLOR:
                radioGroup.check(R.id.hair_color);
                break;
        }

    }

    private void initCloseButton() {
        closeButton = findViewById(R.id.ok);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", selectedOption);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}
